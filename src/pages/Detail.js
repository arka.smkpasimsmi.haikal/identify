import { Link } from "@react-navigation/native";
import React,{useState,useEffect} from "react";
import {View,StyleSheet,Text,TouchableOpacity,TextInput,Image,TouchableHighlight,Linking,StatusBar, FlatList, ImageBackground,SafeAreaView,Dimensions} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from "axios";
import { ProgressChart } from "react-native-chart-kit";

export default function Detail({route,navigation}){  
    const {id} = route.params  
    const [title, setTitle] = useState("")
    const [releaseDt, setReleaseDt] = useState("")
    const [desc, setDesc] = useState("")
    const [rating, setRating] = useState("")
    const [genre, setGenre] = useState([])
    const [poster, setPoster] = useState("")
    const [imgBck, setImgBck] = useState("")
    const [vote, setVote] = useState("")

    const api_key = '?api_key=cdaab15e0b41d685ef3a1abb6438c129'
    const url = `https://api.themoviedb.org/3/movie/${id}${api_key}`
    const getMovieDetail = () =>{
        axios.get( `${url}`)
        .then((resp)=>{
            const movieDetail =resp.data
            setTitle(movieDetail.original_title)
            setImgBck(movieDetail.backdrop_path)
            setGenre(movieDetail.genres)
            setRating(movieDetail.vote_average)
            setDesc(movieDetail.overview)
            setReleaseDt(movieDetail.release_date)
            setPoster(movieDetail.poster_path)
            setVote(movieDetail.vote_count)
        })
        .catch(err => console.error(`Error : ${error}`))
    }
    const combinedRating = `0.${rating*10}`
    const parsedRating = parseFloat(combinedRating,10)
    const MyProgressChart = () => {
        return (
          <>
            <ProgressChart
              data={[parsedRating]}
              width={100}
              height={100}
              hideLegend={true}
              chartConfig={{
                backgroundColor: 'rgba(0, 0, 0, 0)',
                backgroundGradientFrom: 'rgba(0, 0, 0, 0)',
                backgroundGradientTo: 'rgba(0, 0, 0, 0)',
                color: (opacity = 1) => `rgba(218,165,32,${opacity})`,
                style: {
                  borderRadius: 16,
                },
              }}
              style={{
                marginVertical: 8,
                borderRadius: 16,
              }}
            />
          </>
        );
      };
    useEffect(()=>{
        getMovieDetail()
    },[])
    return(
        <View style={{flexDirection:"column",flex:1,backgroundColor:'white',paddingTop:StatusBar.currentHeight}}>
            <View style={{justifyContent:"center",height:60,alignItems:"center",backgroundColor:'#97D5FE',borderBottomLeftRadius:70,borderBottomRightRadius:70}}>
                <Text style={{marginTop:13,fontSize:22,color:'#fff'}}>Movie Detail</Text>
            </View>
            <TouchableOpacity style={{width:55,height:55,borderRadius:100/2,backgroundColor:'#31597D',marginTop:-30,marginLeft:20,justifyContent:'center',alignItems:"center"}} onPress={()=>navigation.goBack()}>
                {/* <Icon name="long-arrow-left" size={30} color="white" /> */}
                <Text style={{color:"white"}}>Back</Text>
            </TouchableOpacity>
            <View style={{flex:1}}>
                 {/* <View> */}
                    <ImageBackground resizeMode="contain" style={{flex:1,justifyContent:"center"}} source={{uri:`https://image.tmdb.org/t/p/original${imgBck}`}} blurRadius={2}>
                        {/* <Text>{imgBck}</Text> */}
                        <View style={{width:'100%',height:400,backgroundColor:'rgba(52, 52, 52, 0.5)'}}>
                            {/* <Text>RANDOM TEXT</Text> */}
                            <View style={{width:324,height:300,flexDirection:'row'}}>
                                <View style={{alignItems:"flex-start"}}>
                                    <Image style={{width:130,height:230,marginHorizontal:15,resizeMode:"contain"}} source={{uri:`https://image.tmdb.org/t/p/original${poster}`}} />
                                </View>
                                   
                                <View style={{flexDirection:'column',width:230,marginLeft:14,alignContent:"center"}}>
                                    <Text numberOfLines={2} style={{fontSize:25,marginTop:20,fontWeight:"bold",color:'white',width:150}}>{title}</Text>
                                    <Text style={{fontSize:12,color:'#E4D8D8',fontStyle:"italic"}}>{releaseDt}</Text>
                                    <MyProgressChart/>
                                    <Text style={{color:'white',fontSize:15}}>Rating : <Text style={{fontWeight:"bold"}}>{rating}/10</Text></Text>
                                    <Text style={{color:'white',fontSize:15}}>Vote : <Text style={{fontWeight:"bold"}}>{vote} users</Text></Text>
                                </View>
                            </View>
                                <View style={{width:'100%',height:200,backgroundColor:'rgba(52, 52, 52, 0.8)',flexDirection:"column",marginTop:-50}}>
                                <Text style={{fontSize:15,color:'white',marginLeft:20,width:310,marginTop:10}}>{desc}</Text>
                                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                  {/* <TouchableOpacity style={[styles.btn,{marginTop:0}]}>
                                      <Text style={{color:'#fff',fontSize:18}} >Add to Favorite</Text>
                                  </TouchableOpacity> */}
                                  {/* <TouchableOpacity style={[styles.btn,{marginTop:0}]}>
                                      <Text style={{color:'#fff',fontSize:18}} >Add to Favorite</Text>
                                  </TouchableOpacity> */}
                                </View>
                                </View>
                        </View>
                    </ImageBackground>
                 {/* </View> */}
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
      },
    btnSelected:{
        margin:5,
        width:100,
        height:32,
        backgroundColor:'#97D5FE',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    btnNotSelected:{
        margin:5,
        width:100,
        height:32,
        backgroundColor:'#D8EFFF',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    header: {
        textAlign: 'center',
        fontSize: 18,
        padding: 16,
        marginTop: 16,
      },
      btn:{
        margin:5,
        width:300,
        height:40,
        backgroundColor:'#67B1E9',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
})