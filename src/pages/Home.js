import { Link } from "@react-navigation/native";
import React,{useState,useEffect} from "react";
import {View,StyleSheet,Text,TouchableOpacity,TextInput,Image,TouchableHighlight,Linking,StatusBar, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from "axios";
import firebase from 'firebase'

export default function Home({navigation}){    
    const api_key = 'api_key=cdaab15e0b41d685ef3a1abb6438c129'
    const [btnSelected, setBtnSelected] = useState(1)
    const [title, setTitle] = useState("")
    const [date, setDate] = useState("")
    const [desc, setDesc] = useState("")
    const [imgurl, setImgurl] = useState("")
    const [items, setItems] = useState([])
    const [items2, setItems2] = useState([])
    const [apiUrl, setApiUrl] = useState("movie/popular?")
    const [userUid, setUserUid] = useState("")
    const [movieQuery, setMovieQuery] = useState("")

    const GetData =()=>{
        axios.get(`https://api.themoviedb.org/3/${apiUrl}${api_key}`)
        .then(res=>{
            const data1=(res.data.results)
            setItems(data1)
        }).catch(err=>{
            console.log('err'),err;
        })
    }

    useEffect(()=>{
        GetData()
        setUserUid(firebase.auth().currentUser.uid)
    },[apiUrl])
    
    return(
        <View style={{flexDirection:"column",flex:1,backgroundColor:'white',paddingTop:StatusBar.currentHeight}}>
            <View style={{justifyContent:"center",height:60,alignItems:"center",backgroundColor:'#97D5FE',borderBottomLeftRadius:70,borderBottomRightRadius:70}}>
                <Text style={{marginTop:13,fontSize:22,color:'#fff'}}>Home</Text>
            </View>
            <View style={{alignItems:"center",marginTop:20}}>
                <View style={{width:300,
                        flexDirection:'row',
                        height:40,
                        borderWidth:2,
                        borderColor:'#31597D',
                        borderRadius:50,
                        justifyContent:"space-between",
                        }}>
                    <TextInput
                        style={{
                            margin:10,
                            width:200,
                        }}
                        value={movieQuery}
                        onChangeText={(value)=>setMovieQuery(value)}
                        placeholder={"Search..."}
                    /> 
                    <TouchableOpacity style={(btnSelected == 4)?styles.btnSelectedFind:styles.btnNotSelectedFind} onPress={()=>{setBtnSelected(4),setApiUrl(`search/movie?query=${movieQuery}&language=en-US&include_adult=false&`),console.log(movieQuery)}}>
                        <Text style={{color:'#fff'}} >Find</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{flexDirection:'row',justifyContent:"center"}}>
                <TouchableOpacity style={(btnSelected==1)?styles.btnSelected:styles.btnNotSelected} onPress={()=> {setBtnSelected(1),setMovieQuery(""),setApiUrl("movie/popular?")}}>
                    <Text style={{color:'white'}}>Popular</Text>
                </TouchableOpacity>
                <TouchableOpacity style={(btnSelected==2)?styles.btnSelected:styles.btnNotSelected} onPress={()=> {setBtnSelected(2),setMovieQuery(""),setApiUrl("movie/top_rated?")}}>
                    <Text style={{color:'white'}}>Top Rated</Text>
                </TouchableOpacity>
                <TouchableOpacity style={(btnSelected==3)?styles.btnSelected:styles.btnNotSelected} onPress={()=> {setBtnSelected(3),setMovieQuery(""),setApiUrl("trending/movie/week?")}}>
                    <Text style={{color:'white'}}>Trending</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={items}
                keyExtractor={(item,index) => `${item.id}-${index}`}
                extraData={items}
                renderItem={({item})=>{
                    return(
                        <TouchableOpacity style={{alignItems:'center',paddingVertical:5}} onPress={()=>{navigation.push('Detail',{
                            id : item.id
                        })}}>
                            <View style={{backgroundColor:'#D8EFFF',width:324,height:116,flexDirection:'row'}}>
                                <View style={{alignItems:"flex-start"}}>
                                    <Image style={{width:76.96,height:116,resizeMode:"contain"}} source={{uri:`https://image.tmdb.org/t/p/original${item.poster_path}`}} />
                                </View>
                                <View style={{flexDirection:'column',width:230,marginLeft:14,justifyContent:"center"}}>
                                    <Text style={{fontSize:16,fontWeight:"bold"}}>{item.title}</Text>
                                    <Text style={{fontSize:10,fontStyle:"italic"}}>{item.release_date}</Text>
                                    <Text numberOfLines={5} style={{fontSize:10,marginRight:10,width:210}}>{item.overview}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
      },
    btnSelected:{
        margin:5,
        width:100,
        height:32,
        backgroundColor:'#97D5FE',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    btnNotSelected:{
        margin:5,
        width:100,
        height:32,
        backgroundColor:'#D8EFFF',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    btn:{
        marginTop:2,
        marginRight:10,
        width:60,
        height:30,
        backgroundColor:'#97D5FE',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    btnSelectedFind:{
        marginTop:2,
        marginRight:10,
        width:60,
        height:30,
        backgroundColor:'#97D5FE',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    btnNotSelectedFind:{
        marginTop:2,
        marginRight:10,
        width:60,
        height:30,
        backgroundColor:'#D8EFFF',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100,
    },
})