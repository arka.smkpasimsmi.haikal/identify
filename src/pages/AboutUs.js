import { Link } from "@react-navigation/native";
import React from "react";
import {View,StyleSheet,Text,TouchableOpacity,TextInput,Image,TouchableHighlight,Linking,StatusBar} from 'react-native'
import { Ionicons } from '@expo/vector-icons';

export default function AboutUs({navigation}){
    return(
        <View style={{flexDirection:"column",flex:1,backgroundColor:'white',paddingTop:StatusBar.currentHeight}}>
            <View style={{flex:0.1,justifyContent:"center",alignItems:"center",backgroundColor:'#97D5FE',borderBottomLeftRadius:70,borderBottomRightRadius:70}}>
                <Text style={{marginTop:13,fontSize:22,color:'#fff'}}>About Us</Text>
            </View> 
            <TouchableOpacity style={{width:55,height:55,borderRadius:100/2,backgroundColor:'#31597D',marginTop:-30,marginLeft:20,justifyContent:'center',alignItems:"center"}} onPress={()=>navigation.goBack()}>
            <Text style={{color:"white"}}>Back</Text>
            </TouchableOpacity>
            {/* <Ionicons name="shrink" size={32} color="green" /> */}
            <View style={{flex:0.2,flexDirection:"row",justifyContent:"space-around"}}>
            <TouchableOpacity style={{borderWidth:5,borderColor:'#B3E0FF',borderRadius:100,width:70,height:70,alignItems:"center",justifyContent:"center",marginLeft:35}} onPress={()=>{Linking.openURL("https://www.facebook.com/Haikal.ranger72")}}>
                <Image style={{width:55,height:55}} source={require('../assets/facebook.png')} />
                
            </TouchableOpacity>
            <TouchableOpacity style={{borderWidth:5,borderColor:'#B3E0FF',borderRadius:100,width:70,height:70,alignItems:"center",justifyContent:"center",marginRight:35}} onPress={()=>{Linking.openURL("https://twitter.com/haikal_mulya80")}}>
                
                <Image style={{width:55,height:55}} source={require('../assets/twitter.png')} />
            </TouchableOpacity>
            </View>
            <View style={{flex:0.1,flexDirection:"row",justifyContent:"space-around"}}>
                <TouchableOpacity style={{borderWidth:5,borderColor:'#B3E0FF',borderRadius:100,width:70,height:70,alignItems:"center",justifyContent:"center"}} onPress={()=>{Linking.openURL("https://gitlab.com/arka.smkpasimsmi.haikal")}}>
                    <Image style={{width:55,height:55}} source={require('../assets/image.png')} />
                </TouchableOpacity>
                <Image style={{width:117,height:122,marginTop:-40,borderWidth:7,borderColor:'#B3E0FF',borderRadius:100}} source={require('../assets/avatar.png')} />
                <TouchableOpacity style={{borderWidth:5,borderColor:'#B3E0FF',borderRadius:100,width:70,height:70,alignItems:"center",justifyContent:"center"}} onPress={()=>{Linking.openURL("https://github.com/haikalputra-dev")}}>
                    <Image style={{width:55,height:55}} source={require('../assets/github.png')} />
                </TouchableOpacity>
            </View>
            <View style={{flex:0.2,justifyContent:"center",alignItems:"center",marginTop:-20}}>
                <Text style={{fontSize:20}}>Muhamad Haikal Mulya Putera</Text>
            </View>
            <View style={{flex:0.1,justifyContent:"center",paddingHorizontal:25,marginTop:70}}>
                <Text style={{fontSize:18}}>
                    <Text style={{color:'#97D5FE'}}>iDentify </Text>  
                    is a digital platform for Movie
                    Fans to discover movies and shows their 
                    favorite movie. So they can <Text style={{color:'#97D5FE'}}>iDentify </Text> 
                    each
                        other favorite movie easily.</Text>
                <Text style={{fontSize:15,marginTop:10}}>
                    My other projects can be seen here : 
                    {"\n"}{"\n"}School Profile
                    {"\n"}
                    <Text style={{color :'#0066FF',textDecorationLine:'underline'}} onPress={()=>{Linking.openURL("https://gitlab.com/arka.smkpasimsmi.axa/regna-smp")}}>https://gitlab.com/arka.smkpasimsmi.axa/regna-smp</Text>
                    {"\n"}E-Learning
                    {"\n"}
                    <Text style={{color :'#0066FF',textDecorationLine:'underline'}} onPress={()=>{Linking.openURL("https://gitlab.com/arka.smkpasimsmi.axa/sisfo")}}>https://gitlab.com/arka.smkpasimsmi.axa/sisfo</Text>
                    {"\n"}Notaris Management
                    {"\n"}
                    <Text style={{color :'#0066FF',textDecorationLine:'underline'}} onPress={()=>{Linking.openURL("https://gitlab.com/abah/notaris")}}>https://gitlab.com/abah/notaris</Text>
                </Text>
            </View>
        </View>
    )
}

{/* <View style={{width:100,height:100,borderRadius:150/2,backgroundColor:'#000'}}/> */}