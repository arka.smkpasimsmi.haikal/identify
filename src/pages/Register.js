import { Link } from "@react-navigation/native";
import React,{useState,useEffect} from "react";
import {View,StyleSheet,Text,TouchableOpacity,TextInput,Image, ImageBackground, SafeAreaView,StatusBar,Dimensions,Alert} from 'react-native'
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import firebase from 'firebase'

export default function Register({navigation}) {

    const firebaseConfig = {
        apiKey: "AIzaSyBZlj5cm1BXCsaQs0Evqbx9WS66AH45DBw",
        authDomain: "identify-72.firebaseapp.com",
        projectId: "identify-72",
        storageBucket: "identify-72.appspot.com",
        messagingSenderId: "489870014857",
        appId: "1:489870014857:web:230a9775e9f080ec2ac958",
        measurementId: "G-WM0WSHQX21"
      };
     if(!firebase.apps.length){
         firebase.initializeApp(firebaseConfig)
     }
      const [email, setEmail] = useState("");
      const [password, setPassword] = useState("");
      const [passwordConfirm, setPasswordConfirm] = useState("");

      const checkData=()=>{
          const dataCheck = {
              email,password,passwordConfirm
          }
          
      } 

      const submit=()=>{
          const data = {
              email,
              password
          }
          if (password === passwordConfirm) {
                console.log(data)
                firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
                console.log('Register Berhasil');
                navigation.navigate("Login");
            }).catch(()=>{
                console.log("register gagal")
            })
          } else {
                Alert.alert("Error",'Password confirmation not matched',[{text:"close"}])
          }   
      }
    return(
        <SafeAreaView style={[styles.container,{backgroundColor:"white",flexDirection:"column",marginTop:StatusBar.currentHeight+20}]}>
            <ImageBackground resizeMode="contain" style={{position:"absolute",flex:1,justifyContent:"flex-start",marginTop:-60,width:Dimensions.get('window').width,height:Dimensions.get("window").height}} source={require('../assets/background.png')}>
            <View style={{alignItems:'flex-start',justifyContent:"center",marginTop:50,flexDirection:'row'}}>
                    <View>
                        <Text style={{fontSize:48,color:'white',fontWeight:"bold"}}>iDentify</Text>
                        <Text style={{fontSize:13,fontWeight:"bold"}}>Discover movies,share to others</Text>
                    </View>
            </View>
            <View style={{flexDirection:'row'}}>
                <View style={{marginLeft:23,marginTop:100}}>
                    <Text style={{fontSize:36}}>Register</Text>
                </View>
                <View style={{alignItems:"flex-end",flex:1,marginRight:10}}>
                    <Image style={{height:200,width:190,resizeMode:'contain'}} source={require('../assets/iDentify-icon.png')}></Image>
                </View>
            </View>
            <View style={{justifyContent:'flex-start',alignItems:'flex-start',paddingHorizontal:23,flexDirection:"column",marginTop:-35}}>
                <TextInput 
                style={[styles.input,{marginBottom:0}]} 
                placeholder="Email/Username"
                value={email}
                onChangeText={(value)=>setEmail(value)}
                />
                <TextInput 
                secureTextEntry={true}
                style={styles.input} 
                placeholder="Password"
                value={password}
                onChangeText={(value)=>setPassword(value)}
                />
                <TextInput 
                secureTextEntry={true}
                style={styles.input} 
                placeholder="Password Confirmation"
                value={passwordConfirm}
                onChangeText={(value)=>setPasswordConfirm(value)}
                />
            <View style={{alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity style={[styles.btn,{marginTop:50}]} onPress={submit}>
                    <Text style={{color:'#fff'}}>Register</Text>
                </TouchableOpacity>
                <Text style={{marginTop:20,textDecorationLine:'underline'}} onPress={()=> navigation.navigate("Login")}>Login</Text>
            </View>
        </View>
        <View style={{alignItems:"center",flex:1,justifyContent:"flex-end"}}>
            <Text style={{textDecorationLine:'underline',marginBottom:10}} onPress={()=>navigation.push("AboutUs")}>About Us</Text>
        </View>
            </ImageBackground>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1
      },
    btn:{
        margin:5,
        width:219,
        height:32,
        backgroundColor:'#97D5FE',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:100
    },
    input: {
        width:223,
        height: 40,
        margin: 12,
        borderBottomWidth: 1,
        borderColor:'#97D5FE',
        paddingHorizontal: 10,
      },
})