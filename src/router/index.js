import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Login from '../pages/Login';
import Register from '../pages/Register';
import Home from '../pages/Home';
import Detail from '../pages/Detail';
import AboutUs from '../pages/AboutUs';
import Profile from '../pages/Profile';

const Stack = createStackNavigator();

export default function Router() {
    return (
        
            <Stack.Navigator>
                <Stack.Screen component={Login} name="Login" options={{ headerShown: false }}/>
                <Stack.Screen component={Register} name="Register" options={{ headerShown: false }}/>
                <Stack.Screen component={Home} name="Home" options={{ headerShown: false }}/>
                <Stack.Screen component={Detail} name="Detail" options={{ headerShown: false }}/>
                <Stack.Screen component={AboutUs} name="AboutUs" options={{ headerShown: false }}/>
                <Stack.Screen component={Profile} name="Profile" options={{ headerShown: false }}/>
            </Stack.Navigator>
    )
}



