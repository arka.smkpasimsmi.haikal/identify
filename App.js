import { StatusBar } from 'expo-status-bar';
import React from 'react';
import 'react-native-gesture-handler'
import { StyleSheet, Text, View } from 'react-native';
import Source from './src'
import * as firebase from 'firebase';

export default function App() {
//   const firebaseConfig = {
//     apiKey: "AIzaSyBZlj5cm1BXCsaQs0Evqbx9WS66AH45DBw",
//     authDomain: "identify-72.firebaseapp.com",
//     projectId: "identify-72",
//     storageBucket: "identify-72.appspot.com",
//     messagingSenderId: "489870014857",
//     appId: "1:489870014857:web:230a9775e9f080ec2ac958"
//   };
//   if(!firebase.apps.length){
//     firebase.initializeApp(firebaseConfig);
// }
  return (
    <Source />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
